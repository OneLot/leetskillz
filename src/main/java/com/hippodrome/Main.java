package com.hippodrome;

import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import java.util.regex.*;
import java.util.stream.*;

import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

class Result {

    /*
     * Complete the 'howMany' function below.
     *
     * The function is expected to return an INTEGER.
     * The function accepts STRING sentence as parameter.
     */

    public static int howMany(String sentence) {

        List<String> validWords = new ArrayList<String>();

        List<String> words = Arrays.asList(sentence.split(" "));
        ArrayList<String> punctuation = new ArrayList<String>();
        punctuation.add("?");
        punctuation.add(".");
        punctuation.add(",");
        punctuation.add("!");

        List<String> romanAlphabet = new ArrayList<String>();
        romanAlphabet.add("A");
        romanAlphabet.add("B");
        romanAlphabet.add("C");
        romanAlphabet.add("D");
        romanAlphabet.add("E");
        romanAlphabet.add("F");
        romanAlphabet.add("G");
        romanAlphabet.add("H");
        romanAlphabet.add("I");
        romanAlphabet.add("J");
        romanAlphabet.add("K");
        romanAlphabet.add("L");
        romanAlphabet.add("M");
        romanAlphabet.add("N");
        romanAlphabet.add("O");
        romanAlphabet.add("P");
        romanAlphabet.add("Q");
        romanAlphabet.add("R");
        romanAlphabet.add("S");
        romanAlphabet.add("T");
        romanAlphabet.add("U");
        romanAlphabet.add("V");
        romanAlphabet.add("W");
        romanAlphabet.add("X");
        romanAlphabet.add("Y");
        romanAlphabet.add("Z");

        int illegalSequence = 0;
        int wordCount = 0;

        for (String elem : words) {
            try {
                Integer aNumber = Integer.valueOf(elem);
                illegalSequence += 1;
                continue;
            } catch (NumberFormatException e) {
                int lengthOfString = elem.length();
                if(lengthOfString == 0) {
                    continue;
                }

                String lastCharacter = String.valueOf(elem.charAt(lengthOfString - 1));

                if (punctuation.contains(lastCharacter)) {
                    elem = elem.substring(0, lengthOfString - 1);
                }
                boolean isWord = true;

                for (int i = 0; i < elem.length(); i++) {

                    char c = elem.charAt(i);
                    String upperChar = String.valueOf(c).toUpperCase();
                    if (!romanAlphabet.contains(upperChar)){
                        if (!upperChar.equals("-")){
                            isWord = false;
                        }
                    }
                }
                if (isWord) {
                    wordCount += 1;
                    validWords.add(elem);
                }
            }
        }

        return wordCount;
    }

}

class Solution {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));
        String sentence = bufferedReader.readLine();

//        String sentence = "he is a good programmer, he won 865 competitions, but sometimes he dont. What do you think? All test-cases should pass. Done-done?";

        int result = Result.howMany(sentence);
        System.out.println(result);
        bufferedWriter.write(String.valueOf(result));
        bufferedWriter.newLine();
        bufferedReader.close();
        bufferedWriter.close();
    }
}
