package com.hippodrome.leetcode.easy.problem202;

public class Main {

    public static void main(String[] args) {
        Solution solution = new Solution();
        System.out.printf("19 is a %b happy number%n", solution.isHappy(19));
        System.out.printf("2 is a %b happy number%n", solution.isHappy(2));
        System.out.printf("234 is a %b happy number%n", solution.isHappy(234));
        System.out.printf("116 is a %b happy number%nn", solution.isHappy(116));

        int happyNumbers = 0;
        for(int n = 1; n < 900; n++) {
            boolean nIsHappyNumber = solution.isHappy(n);
            System.out.printf("%d is a %b happy number%n", n, nIsHappyNumber);
            if(nIsHappyNumber) happyNumbers++;
        }

        System.out.printf("899 checked and %d happy numbers", happyNumbers);
    }

}
