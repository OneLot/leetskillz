package com.hippodrome.leetcode.easy.problem1290;

public class Solution {

    public int getDecimalValue(ListNode head) {
        String s = String.valueOf(head.val);
        if(head.next != null) {
            do {
                s += String.valueOf(head.next.val);
                head = head.next;
            } while (head.next != null);
        }
        return binaryToInteger(s);
    }

    private int binaryToInteger(String binaryString){
        return Integer.parseInt(binaryString, 2);
    }

}
