package com.hippodrome.leetcode.easy.problem1290;

public class Main {
    public static void main(String[] args){
        Solution solution = new Solution();
        ListNode one = new ListNode(1);
        one.next = new ListNode(0);
        one.next.next = new ListNode(1);
        System.out.printf("output=%d for input=" + one.toString() + "%n", solution.getDecimalValue(one));

        ListNode four = new ListNode(1);
        four.next = new ListNode(0);
        four.next.next = new ListNode(0);
        four.next.next.next = new ListNode(1);
        four.next.next.next.next = new ListNode(0);
        four.next.next.next.next.next = new ListNode(0);
        four.next.next.next.next.next.next = new ListNode(1);
        four.next.next.next.next.next.next.next = new ListNode(1);
        four.next.next.next.next.next.next.next.next = new ListNode(1);
        four.next.next.next.next.next.next.next.next.next = new ListNode(0);
        four.next.next.next.next.next.next.next.next.next.next = new ListNode(0);
        four.next.next.next.next.next.next.next.next.next.next.next = new ListNode(0);
        four.next.next.next.next.next.next.next.next.next.next.next.next = new ListNode(0);
        four.next.next.next.next.next.next.next.next.next.next.next.next.next = new ListNode(0);
        four.next.next.next.next.next.next.next.next.next.next.next.next.next.next = new ListNode(0);

        System.out.printf("output=%d for input=" + four.toString()+ "%n", solution.getDecimalValue(four));

    }
}
