package com.hippodrome.leetcode.easy.problem509;

public class Solution {
    public int fib(int n) {

        int[] lastTwo = {0, 1};
        if(n < 2) return lastTwo[n];
        for(int i = 2; i <= n; i++) {
            int last = lastTwo[1];
            lastTwo[1] = lastTwo[1] + lastTwo[0];
            lastTwo[0] = last;
        }
        return lastTwo[1];
    }
}
