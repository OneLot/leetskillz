package com.hippodrome.leetcode.easy.problem453;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Solution solution = new Solution();
        int[] first = {1, 2, 3};
        int[] second = {1, 1, 1};
        System.out.printf("Output=%d for input=%s%n", solution.minMoves(first), Arrays.toString(first));
        System.out.printf("Output=%d for input=%s%n", solution.minMoves(second), Arrays.toString(second));
    }
}
