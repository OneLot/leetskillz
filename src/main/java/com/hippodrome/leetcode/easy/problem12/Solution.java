package com.hippodrome.leetcode.easy.problem12;

import java.util.HashMap;

public class Solution {

    /*
        Symbol       Value
        I             1
        V             5
        X             10
        L             50
        C             100
        D             500
        M             1000

        Example 1:
        Input: num = 3
        Output: "III"

        Example 2:
        Input: num = 4
        Output: "IV"

        Example 3:
        Input: num = 9
        Output: "IX"

        Example 4:
        Input: num = 58
        Output: "LVIII"
        Explanation: L = 50, V = 5, III = 3.

        Example 5:
        Input: num = 1994
        Output: "MCMXCIV"
        Explanation: M = 1000, CM = 900, XC = 90 and IV = 4.

     */
    public String intToRoman(int num) {
        HashMap<Integer, String> romanByInteger = new HashMap<Integer, String>();
        romanByInteger.put(1, "I");
        romanByInteger.put(5, "V");
        romanByInteger.put(10, "X");
        romanByInteger.put(50, "L");
        romanByInteger.put(100, "C");
        romanByInteger.put(500, "D");
        romanByInteger.put(1000, "M");

        int numDigits = (int) (Math.log10(num) + 1);

        StringBuilder numeral = new StringBuilder();
        for (int i = 0; i < numDigits; i++) {
            Integer place = (int) Math.pow(10, (numDigits - i - 1));
            int firstNum = num / place;
            if (firstNum == 4) {
                numeral.append(romanByInteger.get(place));

                numeral.append(romanByInteger.get(place * 5));
            } else if (firstNum == 9) {
                numeral.append(romanByInteger.get(place));
                numeral.append(romanByInteger.get(place * 10));
            } else {
                if (firstNum > 4) {
                    numeral.append(romanByInteger.get(place * 5));
                }
                for (int ii = 0; ii < firstNum % 5; ii++) {
                    numeral.append(romanByInteger.get(place));
                }
            }
            num -= firstNum * place;
        }
        return numeral.toString();
    }
}
