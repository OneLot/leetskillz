package com.hippodrome.leetcode.easy.problem9;

public class Solution {
    public boolean isPalindrome(int x) {
        char[] array = String.valueOf(x).toCharArray();
        boolean palindrome = true;
        for (int i = 0; i < array.length; i++) {
            char inner = array[i];
            char outer = array[array.length - (i + 1)];
            if (Character.compare(inner, outer) != 0) {
                palindrome = false;
                break;
            }
        }
        return palindrome;
    }
}
