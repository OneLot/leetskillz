package com.hippodrome.leetcode.easy.problem7;

class Solution {
    public int reverse(Integer x) {
        String number = String.valueOf(x);
        char[] numberArray = number.toCharArray();
        String inverseNumber = "";

        for (int i = 1; i <= numberArray.length; i++) {
            char numberLiteral = numberArray[numberArray.length - i];
            if (Character.compare(numberLiteral, '-') != 0) {
                inverseNumber += numberArray[numberArray.length - i];
            }
        }
        try {
            if (Math.pow(-2, 31) < Integer.parseInt(inverseNumber) && Integer.parseInt(inverseNumber) < Math.pow(2, 31)) {
                return Integer.parseInt(inverseNumber);
            } else {
                return 0;
            }
        } catch (NumberFormatException e) {
            return 0;
        }

    }
}