package com.hippodrome.leetcode.easy.problem7;

public class Main {
    public static void main(String[] args) {
        Solution solution = new Solution();
        int reverseInt = solution.reverse(43);
        System.out.printf("Tested 43 got %d%n", reverseInt);

        reverseInt = solution.reverse(324234);
        System.out.printf("Tested 324234 got %d%n", reverseInt);

        reverseInt = solution.reverse(-13454215);
        System.out.printf("Tested -13454215 got %d%n", reverseInt);

        reverseInt = solution.reverse(1534236469);
        System.out.printf("Tested 1_534_236_469 got %d%n", reverseInt);
    }
}

