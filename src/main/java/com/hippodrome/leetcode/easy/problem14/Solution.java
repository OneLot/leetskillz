package com.hippodrome.leetcode.easy.problem14;

public class Solution {
    public String longestCommonPrefix(String[] strs) {
        if (strs.length == 0) {
            return "";
        }

        int minLength = 200;

        for (String substring : strs) {
            if (minLength > substring.length()) {
                minLength = substring.length();
            }
        }

        String prefix = "";
        outterLoop:
        for (int i = 0; i < minLength; i++) {
            char characterToCheck = 0;
            boolean isFirstCheck = true;
            boolean shouldAdd = true;


            for (String substring : strs) {
                if (isFirstCheck) {
                    characterToCheck = substring.charAt(i);
                } else {
                    if (Character.compare(substring.charAt(i), characterToCheck) != 0) {
                        shouldAdd = false;
                        break outterLoop;
                    }
                }
                isFirstCheck = false;
            }
            if (shouldAdd) {
                prefix += characterToCheck;
            }
        }
        return prefix;
    }
}
