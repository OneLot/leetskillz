package com.hippodrome.leetcode.easy.problem14;

public class Main {
    public static void main(String[] args) {
        //["flower","flow","flight"]

        Solution solution = new Solution();
        String longestPrefix = solution.
                longestCommonPrefix(new String[]{"cir", "car"});
        System.out.println(longestPrefix);
    }
}
