package com.hippodrome.leetcode.easy.problem53;

public class Solution {
    // Naive O(n^2)
    public int maxSubArrayNaive(int[] nums) {
        int largestSum = -100000;
        for (int i = 0; i < nums.length; i++) {
            int sum = nums[i];
            if(largestSum < sum) {
                largestSum = sum;
            }
            for (int j = i + 1; j < nums.length; j++) {
                sum += nums[j];
                if(largestSum < sum) {
                    largestSum = sum;
                }
            }
        }
        return largestSum;
    }

    /*  Implementing Divide and conquer
     *
     *  Let's follow here a solution template for the divide and conquer problems :
     *  Define the base case(s).
     *  Split the problem into subproblems and solve them recursively.
     *  Merge the solutions for the subproblems to obtain the solution for the original problem.
     */
    // O(N log N)
    public int crossSum(int[] nums, int left, int right, int p) {
        if (left == right) return nums[left];

        int leftSubsum = Integer.MIN_VALUE;
        int currSum = 0;
        for(int i = p; i > left - 1; --i) {
            currSum += nums[i];
            leftSubsum = Math.max(leftSubsum, currSum);
        }

        int rightSubsum = Integer.MIN_VALUE;
        currSum = 0;
        for(int i = p + 1; i < right + 1; ++i) {
            currSum += nums[i];
            rightSubsum = Math.max(rightSubsum, currSum);
        }

        return leftSubsum + rightSubsum;
    }

    public int helper(int[] nums, int left, int right) {
        if (left == right) return nums[left];

        int p = (left + right) / 2;
        int leftSum = helper(nums, left, p);
        int rightSum = helper(nums, p + 1, right);
        int crossSum = crossSum(nums, left, right, p);

        return Math.max(Math.max(leftSum, rightSum), crossSum);
    }

    public int maxSubArray(int[] nums){
        return helper(nums, 0, nums.length - 1);
    }
}
