package com.hippodrome.leetcode.easy.problem53;

public class Main {
    public static void main(String[] args) {
        Solution solution = new Solution();
        System.out.printf("%d%n",solution.maxSubArray(new int[]{-2,1,-3,4,-1,2,1,-5,4}));
    }
}
