package com.hippodrome.leetcode.easy.problem696needsPerformanceTunningWIthGroups;

import java.util.ArrayList;

public class Solution {

    ArrayList<String> memo = new ArrayList<String>();
    public int countBinarySubstrings(String s) {
        int subStrings = 0;

        for (int i = 0; i < s.length(); i++) {
            for (int j = i + 1; j <= s.length(); j++) {
                if (i != j) {
                    String subStringToValidate = s.substring(i, j);
                    if (isBinarySubstring(subStringToValidate)) {
                        subStrings += 1;
                    }
                }
            }
        }
        System.out.printf("Found for string %s, %d, binary substrings= %s%n", s, subStrings, memo.toString());
        return subStrings;
    }

    private boolean isBinarySubstring(String s) {

        char[] splitString = s.toCharArray();
        char firstCharacter = splitString[0];
        int firstCharacterCounts = 1;
        int secondCharacterCounts = 0;

        boolean hasFlipped = false;

        if (splitString.length == 2 && splitString[1] != firstCharacter) {
            memo.add(s);
            return true;
        }

        for (int i = 1; i < splitString.length; i++) {
            if (Character.compare(splitString[i], firstCharacter) == 0) {
                firstCharacterCounts += 1;
                if (hasFlipped) {
                    return false;
                }
            } else {
                secondCharacterCounts += 1;
                hasFlipped = true;
            }
        }
        if(firstCharacterCounts == secondCharacterCounts) {
            return true;
        }
        return false;
    }
}
