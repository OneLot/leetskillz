package com.hippodrome.leetcode.easy.problem20;

import java.util.ArrayList;
import java.util.Stack;

class Solution {
    public boolean isValid(String s) {
        /*
        '(','{','['
         ')','}',']'
         */
        ArrayList<Character> pushChars = new ArrayList<Character>();
        pushChars.add('(');
        pushChars.add('{');
        pushChars.add('[');

        Stack<Character> stack = new Stack<Character>();
        boolean isGood = false;
        outerLoop:
        for (char c : s.toCharArray()) {
            if (pushChars.contains(c)) {
                stack.push(c);
            } else {
                try {
                    char popped = stack.pop();
                    if (Character.compare(popped, '{') == 0) {
                        if (Character.compare(c, '}') == 0) {
                            isGood = true;
                            continue;
                        } else {
                            return false;
                        }
                    }
                    if (Character.compare(popped, '[') == 0) {
                        if (Character.compare(c, ']') == 0) {
                            isGood = true;
                            continue;
                        } else {
                            return false;
                        }
                    }
                    if (Character.compare(popped, '(') == 0) {
                        if (Character.compare(c, ')') == 0) {
                            isGood = true;
                        } else {
                            return false;
                        }
                    }
                } catch (java.util.EmptyStackException e) {
                    isGood = false;
                    break outerLoop;
                }
            }
        }
        if (stack.size() != 0) {
            return false;
        }
        return isGood;
    }
}
