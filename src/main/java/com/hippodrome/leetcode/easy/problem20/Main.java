package com.hippodrome.leetcode.easy.problem20;

public class Main {
    public static void main(String[] args) {
        Solution solution = new Solution();
        System.out.printf("%b%n", solution.isValid("({{{{}}}))"));
    }
}
