package com.hippodrome.leetcode.medium.problem91;

public class Main {
    public static void main(String[] args) {
        Solution solution = new Solution();
        System.out.printf("solution = %d for input=12%n", solution.numDecodings("12"));
        System.out.printf("solution = %d for input=226%n", solution.numDecodings("226"));
        System.out.printf("solution = %d for input=0%n", solution.numDecodings("0"));
        System.out.printf("solution = %d for input=06%n", solution.numDecodings("06"));
    }
}
