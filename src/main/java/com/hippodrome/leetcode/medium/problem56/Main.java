package com.hippodrome.leetcode.medium.problem56;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Solution solution = new Solution();
        int[][] inputOne = new int[][]{
                {1, 3},
                {2, 6},
                {8, 10},
                {15, 18}};
        System.out.printf("Output=%s for input=%s %n",
                Arrays.deepToString(solution.merge(inputOne)),
                Arrays.deepToString(inputOne));

        int[][] inputTwo = new int[][]{
                {1, 4},
                {4, 5}};
        System.out.printf("Output=%s for input=%s %n",
                Arrays.deepToString(solution.merge(inputTwo)),
                Arrays.deepToString(inputTwo));

        int[][] inputThree = new int[][]{
                {2, 3},
                {4, 5},
                {6, 7},
                {8, 9},
                {1, 10}
        };
        System.out.printf("Output=%s for input=%s %n",
                Arrays.deepToString(solution.merge(inputThree)),
                Arrays.deepToString(inputThree));
    }
}
