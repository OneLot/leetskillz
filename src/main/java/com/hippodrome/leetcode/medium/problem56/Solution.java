package com.hippodrome.leetcode.medium.problem56;


import java.util.ArrayList;

/*

Given an array of intervals where intervals[i] = [starti, endi],
merge all overlapping intervals, and return an array of the non-overlapping
intervals that cover all the intervals in the input.

Example 1:

Input: intervals = [[1,3],[2,6],[8,10],[15,18]]
Output: [[1,6],[8,10],[15,18]]
Explanation: Since intervals [1,3] and [2,6] overlaps, merge them into [1,6].
Example 2:

Input: intervals = [[1,4],[4,5]]
Output: [[1,5]]
Explanation: Intervals [1,4] and [4,5] are considered overlapping.

 */
public class Solution {

    public class Interval implements Comparable<Interval> {

        public int lowerBound;
        public int upperBound;

        public Interval(int lowerBound, int upperBound) {
            this.lowerBound = lowerBound;
            this.upperBound = upperBound;
        }

        public boolean isIntersect(Interval other) {
            if (other.lowerBound > this.upperBound || this.lowerBound > other.upperBound) return false;
            return true;
        }

        public Interval mergeWithOther(Interval other) {
            int minLowBound = Math.min(this.lowerBound, other.lowerBound);
            int upperBound = Math.max(this.upperBound, other.upperBound);
            return new Interval(minLowBound, upperBound);
        }

        public int[] toIntArray() {
            return new int[]{this.lowerBound, this.upperBound};
        }

        @Override
        public int compareTo(Interval o) {
            return Double.compare(this.lowerBound, o.lowerBound);
        }
    }


    public int[][] merge(int[][] intervals) {
        ArrayList<Interval> intervalList = new ArrayList<Interval>();

        for (int[] interval : intervals) {
            intervalList.add(new Interval(interval[0], interval[1]));
        }

        intervalList.sort(Interval::compareTo);
        int checkStack = intervalList.size();

        for (int i = 0; i < checkStack - 1; i++) {
            Interval nthInterval = intervalList.get(i);
            Interval nextInterval = intervalList.get(i + 1);

            if (nthInterval.isIntersect(nextInterval)) {
                intervalList.remove(nthInterval);
                intervalList.remove(nextInterval);
                intervalList.add(i, nthInterval.mergeWithOther(nextInterval));
                i = i - 1;
                checkStack -= 1;
            }
        }

        return intervalList.stream().map(Interval::toIntArray).toArray(int[][]::new);
    }

}
