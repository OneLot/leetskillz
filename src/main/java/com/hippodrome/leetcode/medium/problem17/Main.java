package com.hippodrome.leetcode.medium.problem17;

public class Main {
    public static void main(String[] args){
        Solution solution = new Solution();
        System.out.printf("output=%s for input=%s%n", solution.letterCombinations("23"), "23");
    }
}
