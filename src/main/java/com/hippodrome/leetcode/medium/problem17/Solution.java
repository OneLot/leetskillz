package com.hippodrome.leetcode.medium.problem17;

import java.util.*;

public class Solution {
    List<String> combinations = new ArrayList<>();
    Map<Character, String> lettersByNumber = Map.ofEntries(
            new AbstractMap.SimpleEntry<Character, String>('2', "abc"),
            new AbstractMap.SimpleEntry<Character, String>('3', "def"),
            new AbstractMap.SimpleEntry<Character, String>('4', "ghi"),
            new AbstractMap.SimpleEntry<Character, String>('5', "jkl"),
            new AbstractMap.SimpleEntry<Character, String>('6', "mno"),
            new AbstractMap.SimpleEntry<Character, String>('7', "pqrs"),
            new AbstractMap.SimpleEntry<Character, String>('8', "tuv"),
            new AbstractMap.SimpleEntry<Character, String>('9', "wxyz"));
    private String phoneDigits;

    public List<String> letterCombinations(String digits) {
        // If the input is empty, immediately return an empty answer array
        if (digits.length() == 0) {
            return combinations;
        }

        // Initiate backtracking with an empty path and starting index of 0
        phoneDigits = digits;
        backtrack(0, new StringBuilder());
        return combinations;
    }

    private void backtrack(int index, StringBuilder path) {
        // If the path is the same length as digits, we have a complete combination
        if (path.length() == phoneDigits.length()) {
            combinations.add(path.toString());
            return; // Backtrack
        }

        // Get the letters that the current digit maps to, and loop through them
        String possibleLetters = lettersByNumber.get(phoneDigits.charAt(index));
        for (char letter: possibleLetters.toCharArray()) {
            // Add the letter to our current path
            path.append(letter);
            // Move on to the next digit
            backtrack(index + 1, path);
            // Backtrack by removing the letter before moving onto the next
            path.deleteCharAt(path.length() - 1);
        }
    }
}
