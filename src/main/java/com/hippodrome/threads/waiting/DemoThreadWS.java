package com.hippodrome.threads.waiting;

import org.apache.commons.lang3.exception.ExceptionUtils;

import java.util.logging.Logger;

class DemoThreadWS implements Runnable {
    private final static Logger LOGGER = Logger.getLogger(DemoThreadWS.class.getName());

    public void run() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            LOGGER.severe("Thread interrupted " + ExceptionUtils.getStackTrace(e));
        }

        LOGGER.info(WaitingState.t1.getState().name());
    }
}