package com.hippodrome.threads.waiting;

import org.apache.commons.lang3.exception.ExceptionUtils;

import java.util.logging.Logger;


public class WaitingState implements Runnable {
    public static Thread t1;
    private final static Logger LOGGER = Logger.getLogger(WaitingState.class.getName());

    public static void main(String[] args) {
        t1 = new Thread(new WaitingState());
        t1.start();
    }

    public void run() {
        Thread t2 = new Thread(new DemoThreadWS());
        t2.start();

        try {
            t2.join();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            LOGGER.info("Thread interrupted " + ExceptionUtils.getStackTrace(e));
        }
    }
}