package com.hippodrome.threads.terminated;

import com.hippodrome.threads.timedwaiting.TimedWaitingState;

import java.util.logging.Logger;

public class TerminatedState implements Runnable {
    private final static Logger LOGGER = Logger.getLogger(TerminatedState.class.getName());

    public static void main(String[] args) throws InterruptedException {
        Thread t1 = new Thread(new TerminatedState());
        t1.start();
        // The following sleep method will give enough time for
        // thread t1 to complete
        Thread.sleep(1000);
        LOGGER.info(t1.getState().name());
    }

    @Override
    public void run() {
        // No processing in this block
    }
}