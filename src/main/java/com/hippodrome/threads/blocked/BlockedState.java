package com.hippodrome.threads.blocked;

import java.util.logging.Logger;

public class BlockedState {
    public static void main(String[] args) throws InterruptedException {
        Logger log = Logger.getLogger("my.logger");
        Thread t1 = new Thread(new DemoThreadB());
        Thread t2 = new Thread(new DemoThreadB());

        t1.start();
        t2.start();

        Thread.sleep(1000);

        log.info(t2.getState().name());
        System.exit(0);
    }
}
