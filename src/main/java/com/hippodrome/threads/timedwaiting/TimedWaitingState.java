package com.hippodrome.threads.timedwaiting;

import java.util.logging.Logger;

public class TimedWaitingState {
    private final static Logger LOGGER = Logger.getLogger(TimedWaitingState.class.getName());

    public static void main(String[] args) throws InterruptedException {
        DemoThread obj1 = new DemoThread();
        Thread t1 = new Thread(obj1);
        t1.start();

        // The following sleep will give enough time for ThreadScheduler
        // to start processing of thread t1
        Thread.sleep(1000);
        LOGGER.info(t1.getState().name());
    }
}
