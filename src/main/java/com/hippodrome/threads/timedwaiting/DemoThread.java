package com.hippodrome.threads.timedwaiting;

import java.util.logging.Logger;

class DemoThread implements Runnable {
    private final static Logger LOGGER = Logger.getLogger(DemoThread.class.getName());

    @Override
    public void run() {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            LOGGER.info("Thread interrupted" + e);
        }
    }
}